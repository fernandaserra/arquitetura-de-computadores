#include <iostream>
#include <omp.h>

int main()
{
    int sum = 0;
    // Todas as threads devem executar 
    #pragma omp parallel 
    {
        //todas vao executar
        printf("Todas as threads executam esta instrução! Sou a Thread %d\n", omp_get_thread_num());
        
        #pragma omp barrier
        #pragma omp sections
        {
            //oq se pôr em cada secção só será executar por uma thread
            #pragma omp section
            {
                printf("Section A: Esta section vai ser executada somente pela thread %d !\n", omp_get_thread_num());
                for(int i =0; i<100000; i++)
                {
                    double temp = 10.0 * 5.0;
                }
            }

            #pragma omp section
            {
                printf("Section B: Esta section vai ser executada somente pela thread %d !\n", omp_get_thread_num());
                for(int i =0; i<100000; i++)
                {
                    double temp = 10.0 * 5.0;
                }
            }

            #pragma omp section
            {
                printf("Section C: Ultima secao vai ser executada pela thread %d !\n", omp_get_thread_num());
                for(int i =0; i<100000; i++)
                {
                    double temp = 10.0 * 5.0;
                }
            }
        }
        printf("Por fim, todas as threads executam esta instrução ! Sou a Thread %d\n", omp_get_thread_num());   
        
        #pragma omp critical
        {
            sum += 1;
        } 

    }
    printf("Sum : %d\n", sum);
        
    return 0;
}
