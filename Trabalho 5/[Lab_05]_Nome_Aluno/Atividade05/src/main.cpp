//multiplicação matricial

#include <omp.h>
#include <iostream>
#define N 5

int main() {

    int m1[N][N], m2[N][N], result[N][N]; int aux1 = 10, aux2 = 5;
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
            m1[i][j] = aux1;  m2[i][j] = aux2;
        }
    }

    int aux = 0;
    #pragma omp parallel
    for(int i = 0; i < N; i++)
    {
        for(int j = 0; j < N; j++)
        {
            printf("thread %d irá fazer a multiplicação %d %d\n",omp_get_thread_num(), i,j );
            for(int k = 0; k < N; k++) aux += m1[i][k] * m2[k][j];
            result[i][j] = aux; aux = 0;
        }
    }

    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++) printf("%d ", result[i][j]);
        printf("\n");
    }

    return 0;
}