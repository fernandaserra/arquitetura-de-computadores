//achar maior valor dos três vetores

#include <iostream>
#include<time.h>
#include <omp.h>

void inserirAleatorio(int v[], int n)
{
    for (int i = 0; i < n; i++) v[i] = (rand() % 50);
}


int main() 
{
    int v_maiores[3];

    int *v1, *v2, *v3, n = 10;
    v1 = (int *)malloc(n * sizeof(int)); inserirAleatorio(v1, n);
    v2 = (int *)malloc(n * sizeof(int)); inserirAleatorio(v2, n);
    v3 = (int *)malloc(n * sizeof(int)); inserirAleatorio(v3, n);
    

    #pragma omp parallel
    {
        #pragma omp sections
        {
            
            // compara qual é o maior valor do v1
            #pragma omp section
            {
                v_maiores[0] = v1[0];

                for(int i = 0; i < n; i++)
                {
                    if(v_maiores[0] < v1[i]) v_maiores[0] = v1[i];
                }
            }

            // compara qual é o maior valor do v2
            #pragma omp section
            {
                v_maiores[1] = v2[0];

                for(int i = 0; i < n; i++)
                {
                    if(v_maiores[1] < v2[i]) v_maiores[1] = v2[i];
                }
            }
            
            // compara qual é o maior valor de v3
            #pragma omp section
            {
                v_maiores[2] = v3[0];

                for(int i = 0; i < n; i++)
                {
                    if(v_maiores[2] < v3[i]) v_maiores[2] = v3[i];
                }
            }
        }
    }
    //vetor 1
    for(int i = 0; i < 10; i++) printf("%d ", v1[i]);
    printf("\n");
    //vetor 1
    for(int i = 0; i < 10; i++) printf("%d ", v2[i]);
    printf("\n");
    //vetor 1
    for(int i = 0; i < 10; i++) printf("%d ", v3[i]);
    printf("\n");
    //vetor com os maiores valores de cada vetor
    for(int i = 0; i < 3; i++) printf("%d ", v_maiores[i]);
    printf("\n");

    for(int i = 1; i < 3; i++)
    {
        if(v_maiores[0] < v_maiores[i]) v_maiores[0] = v_maiores[i];
    }
    
    printf("O maior item dos três vetores é %d. \n", v_maiores[0]);

    return 0;
}