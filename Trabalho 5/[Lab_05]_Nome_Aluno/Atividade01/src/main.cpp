/*
#include <stdio.h>

int main()
{

    printf("Hello World!\n");
    
    return 0;
}

#include <stdio.h>

int main()
{
    #pragma omp parallel
    {
        printf("Hello World!\n");
    }
    return 0;
}
*/
#include<omp.h>
#include <stdio.h>

int main()
{
    omp_set_num_threads(4); //setar número de threads
    int np = 0; int iam = 0;

    #pragma omp parallel private(np, iam)
    {
        np = omp_get_num_threads();
        iam = omp_get_thread_num();
        printf("Hello World from thread %d of %d !\n", iam, np);
    }
    return 0;
}