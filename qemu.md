## Comandos usados no QEMU :
* gcc -O1 - g -test.c test
	A flag -O1 permite que o compilador otimize o código, e a -g permite que o compitador gera uma versão desbugada.
* gcc -O1 -S test.c -o teste.s
	A flag -S permite a compilação do código em c para assembly(extensão .s).
* gcc -c teste.s -o test.o
	O assembler transforma o código assembly em código de máquina.
* objdump -S prog.o
	'Disassemblar' o código de máquina para ver o código em assembly. Esse comando faz com que os endereços, endereços/códigos de instruções e código em assembly sejam mostrados, todos de acordo com suas respectivas linhas.  
