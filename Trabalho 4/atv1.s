; Adição de 2 inteiros
.data
    num1: .word64 12
    num2: .word64 4
    result: .word64 0

.text
    lw $t0, num1($v0)
    lw $t1, num2($v0)
    dadd $t2, $t0, $t1
    sd $t2, result($v0)

    syscall 0
