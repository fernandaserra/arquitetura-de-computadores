; Multiplicação de 2 ponto flutuantes

.data 
    num1: .double 0.5
    num2: .double 10
    result1: .double 0.0
    result2: .double 0

.text
    ldc1 f0, num1($t0)
    ldc1 f1, num2($t1)
    mul.d f2, f0, f1
    sdc1 f2, result1($v0)
    cvt.w.d f3, f2
    dmfc1 $t2, f3
    sd $t2, result2($v0)

    syscall 0