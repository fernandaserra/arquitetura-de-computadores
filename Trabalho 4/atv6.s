.data
    escalar: .double 3
    .double 3, 4, 5, 6
    
.text
    addi R1, $zero, 0x08
    addi R2, $zero, 0x28
    l.d f2, 0($zero)
    
    loop:
        l.d f0, 0(R1)
        add.d f4,f0,f2
        s.d f4,0(R1)
        addi R1, R1, 8
        bne R1, R2, loop
    
    syscall 0
