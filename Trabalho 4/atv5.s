; Resolução de uma equação do segundo grau

.data
    a: .word64 2
    b: .word64 2
    c: .word64 3
    x: .word64 4
    y: .word64 1

.text
    lw $t0, a($v0)
    lw $t1, b($v0)
    lw $t2, c($v0)
    lw $t3, x($v0)
    lw $t4, y($v0)

    dmult $t3, $t3
    mflo $t4
    dmult $t4, $t0
    mflo $t4

    dmult $t1, $t3
    mflo $t1 ;reutilização do reg $t1(b) p n precisar usar um aux

    dadd $t4, $t4, $t1

    dadd $t4, $t4, $t2
    sd $t4, y($v0)

    syscall 0
