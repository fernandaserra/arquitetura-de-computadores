; Fatorial de um inteiro

.data
    num1: .word64 5
    aux: .word64 0
    result: .word64 1
    cont: .word64 0

.text
    lw $t0, num1($v0)
    lw $t1, aux($v0)
    lw $t2, result($v0)
    lw $t3, cont($v0)

    movn $t1, $t0, $t2
    for:
        beq $t1, $t3, end
        dmult $t1, $t2
        mflo $t2
        sd $t2, result($v0)

        daddi $t1, $t1, -1
        sd $t1, aux($v0)

        b for
    end:
        sd $t2, result($v0)

    syscall 0
