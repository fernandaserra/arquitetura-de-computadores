O código em assembly consiste de duas partes, uma para dados e outra para texto/código
As instruções estão localizadas no capítulo 2.
* Na primeira atividade será usado o AND e ADD(32 bits), para utilizar a soma de 64 é DADD (double add).
* lw $t0, numero1($v0) esse formato é específico do MIPS.
* SD store
* syscall 0 comando para encerrar a execução

__Atividade 3__
* Existem instruções específicas de load e storage pro ponto flutuante - ldc1, sdc1.
* É necessário desocupar o registrador da unidade de ponto flutuante após usá-lo por isso (dmfc1 $t2, f3)
* cvt.w.d f3, f2 - converte de double para word(int)