.data
    escalar: .double 3
    .double 3, 4, 5, 6

.text
    l.d f2, 0($zero)
    add.d f3, f2, $zero
    addi R1, $zero, 0x08
    addi R2, $zero, 0x28
    
    loop:
        BEQ R1, R2, end
        add.d f3,f3,f2
        s.d f3,0(R1)
        addi R1, R1, 8
        B loop
    end:
      syscall 0
