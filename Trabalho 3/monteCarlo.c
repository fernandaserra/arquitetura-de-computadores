#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#define INT_MAX 2147483647

int rseed = 0;
int rand()
{
  return rseed = (rseed * 1664525 + 1013904223) & INT_MAX;
}

double monteCarlo(int n)
{
  int acertos = 0;
  double x, y, a;

  for(int i = 0; i < n; i++)
  {
    x = ((double)rand()/INT_MAX);
    y = ((double)rand()/INT_MAX);

    if((x*x) + (y*y) < 1) acertos += 1;
  }

  a = (double) acertos/n;
  return 4 * a;
}

int main()
{
    printf("%lf\n", monteCarlo(5000000));

    return 0;
}
