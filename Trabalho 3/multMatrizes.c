#include<stdio.h>
#include<stdlib.h>
#define t 500

void mMatriz(int m, int n, int p, int m1[m][n], int m2[n][p], int r[m][p])
{
  int aux = 0;

  for(int i = 0; i < n; i++)
  {
    for(int j = 0; j < n; j++)
    {
      for(int k = 0; k < n; k++) aux += m1[i][k] * m2[k][j];
      r[i][j] = aux; aux = 0;
    }
  }
}

int main()
{
  int a[t][t], b[t][t], c[t][t];
  int aux = 3;

  for(int i = 0; i < t; i++)
  {
    for(int j = 0; j < t; j++)
    {
      a[i][j] = aux;
      b[i][j]= aux * 3; c[i][j] = aux * 2;
      aux += 10;
    }
  }
  mMatriz(t,t,t, a, b, c);

  return 0;
}
